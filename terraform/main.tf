resource "google_compute_network" "vpc_network" {
  name                    = "wordpress-network"
  auto_create_subnetworks = false
  mtu                     = 1460
}

resource "google_compute_subnetwork" "subnet" {
  name          = "wordpress-subnet"
  ip_cidr_range = "10.0.1.0/24"
  region        = "us-central1"
  network       = google_compute_network.vpc_network.id
}

resource "google_compute_firewall" "ssh" {
  name = "allow-ssh"
  allow {
    ports    = ["22", "80"]
    protocol = "tcp"
  }
  direction     = "INGRESS"
  network       = google_compute_network.vpc_network.id
  priority      = 1000
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["ssh"]
}

resource "google_compute_instance" "wordpress_instance" {
  name         = "wordpress-vm"
  machine_type = "f1-micro"
  zone         = "us-central1-a"
  tags         = google_compute_firewall.ssh.target_tags

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }
  network_interface {
    subnetwork = google_compute_subnetwork.subnet.id
    access_config {}
  }

  metadata = {
    ssh-keys = "${local.ssh_user}:${file(local.public_key_path)}"
  }

}

resource "null_resource" "run_ansible_script" {
  triggers = {
    build_number = timestamp()
  }
  provisioner "remote-exec" {
    inline = ["echo Waiting for SSH to be available"]
    connection {
      type        = "ssh"
      host        = google_compute_instance.wordpress_instance.network_interface.0.access_config.0.nat_ip
      user        = local.ssh_user
      private_key = file(local.private_key_path)
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i ${google_compute_instance.wordpress_instance.network_interface.0.access_config.0.nat_ip}, --private-key ${local.private_key_path} ../ansible/wordpress.yml --ssh-extra-args='-o StrictHostKeyChecking=no' --extra-vars 'db_user=${data.google_secret_manager_secret_version.wp-db-user.secret_data} db_password=${data.google_secret_manager_secret_version.wp-db-password.secret_data} db_name=${data.google_secret_manager_secret_version.wp-db-name.secret_data} db_host=${google_sql_database_instance.instance.ip_address.0.ip_address}'"
  }

  depends_on = [google_compute_instance.wordpress_instance]
}
