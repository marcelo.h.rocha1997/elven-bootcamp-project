data "google_secret_manager_secret_version" "wp-db-user" {
  secret = "wp-db-user"
}

data "google_secret_manager_secret_version" "wp-db-password" {
  secret = "wp-db-password"
}

data "google_secret_manager_secret_version" "wp-db-name" {
  secret = "wp-db-name"
}