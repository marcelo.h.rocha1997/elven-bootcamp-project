resource "random_id" "db_name_suffix" {
  byte_length = 4
}


resource "google_compute_global_address" "private_ip_address" {
  provider = google-beta

  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.vpc_network.id

  depends_on = [google_compute_network.vpc_network]
}

resource "google_service_networking_connection" "private_vpc_connection" {
  provider = google-beta

  network                 = google_compute_network.vpc_network.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]

  depends_on = [google_compute_network.vpc_network, google_compute_global_address.private_ip_address]
}

resource "google_sql_database_instance" "instance" {
  name                = "wordpress-instance-${random_id.db_name_suffix.hex}"
  database_version    = "MYSQL_5_7"
  depends_on          = [google_service_networking_connection.private_vpc_connection]
  deletion_protection = false
  settings {
    tier = "db-f1-micro"
    ip_configuration {
      ipv4_enabled                                  = false
      private_network                               = google_compute_network.vpc_network.id
      enable_private_path_for_google_cloud_services = true

    }
  }
}

resource "google_sql_database" "database" {
  name     = data.google_secret_manager_secret_version.wp-db-name.secret_data
  instance = google_sql_database_instance.instance.name
}

resource "google_sql_user" "users" {
  name     = data.google_secret_manager_secret_version.wp-db-user.secret_data
  instance = google_sql_database_instance.instance.name
  password = data.google_secret_manager_secret_version.wp-db-password.secret_data
}

