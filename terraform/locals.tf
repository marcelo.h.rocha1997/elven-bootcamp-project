locals {
  ssh_user         = "ansible"
  public_key_path  = "./ansible_rsa.pub"
  private_key_path = "./ansible_rsa"
}